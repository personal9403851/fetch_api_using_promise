//Fetch all the todos using fetch API
const fetch = require('node-fetch')

let p = fetch('https://jsonplaceholder.typicode.com/todos')

p.then((response) => {
    if (response.ok === true)
        return response.json()
    else
        console.log(`Error: ${response.status}`)
}).then((jsonData) => {
    if (jsonData != undefined) {
        console.log(jsonData)
    }
}).catch((error) => {
    console.log(error)
})