const fetch = require('node-fetch')

const fetchUsers = () => {

    return new Promise((resolve, reject) => {

        let p = fetch('https://jsonplaceholder.typicode.com/users')

        p.then((response) => {
            if (response.ok === true)
                return response.json()
            else
                console.log(`Error occurred while fetching users: ${response.status}`)
        }).then((jsonData) => {
            if (jsonData != undefined)
                resolve(jsonData)
        }).catch((error) => {
            reject(error)
        })
    })
}


const fetchToDos = () => {

    return new Promise((resolve, reject) => {

        let p = fetch('https://jsonplaceholder.typicode.com/todos')

        p.then((response) => {
            if (response.ok === true)
                return response.json()
            else
                console.log(`Error occurred while fetching to-dos: ${response.status}`)
        }).then((jsonData) => {
            if (jsonData != undefined) {
                resolve(jsonData)
            }
        }).catch((error) => {
            reject(error)
        })
    })
}


fetchUsers().then((usersData) => {

    console.log(`Users data fetched successfully`)
    console.log(usersData)

    fetchToDos().then((toDosData) => {

        console.log(`Todos data fetched successfully`)
        console.log(toDosData)

    }).catch((error) => {

        console.log(error)
    })
}).catch((error) => {
    console.log(error)
})