const fetch = require('node-fetch');

const fetchUsers = () => {
    return new Promise((resolve, reject) => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => {
                if (response.ok === true)
                    return response.json();
                else
                    throw new Error(`Error occurred while fetching users: ${response.status}`);
            })
            .then((jsonData) => {
                resolve(jsonData);
            })
            .catch((error) => {
                reject(error);
            });
    });
};

const fetchDataOfEachUser = (usersData) => {
    let promises = [];

    usersData.forEach(eachUser => {
        let eachUserId = eachUser.id;

        promises.push(new Promise((resolve, reject) => {

            fetch(`https://jsonplaceholder.typicode.com/users/${eachUserId}`)
                .then((response) => {
                    if (response.ok === true)
                        return response.json();
                    else
                        throw new Error(`Error occurred while fetching data for: ${eachUserId}`);
                })
                .then((userData) => {
                    console.log(`Specific data for user having id: ${eachUserId} \n`);
                    console.log(userData);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        }));
    });

    return Promise.allSettled(promises);
};

fetchUsers().then((jsonData) => {
    console.log(`JsonData for all users fetched successfully`);

    fetchDataOfEachUser(jsonData)
        .then(() => {
            console.log(`Specific data for all users fetched successfully`);
        })
        .catch((error) => {
            console.log(error);
        });
}).catch((error) => {
    console.log(error);
});
