const fetch = require('node-fetch')

const fetchToDo = () => {

    return new Promise((resolve, reject) => {

        let p = fetch(`https://jsonplaceholder.typicode.com/todos`)

        p.then((response) => {
            if (response.ok === true)
                return response.json()
            else
                throw new Error(`Error while fetching the ToDos: ${response.status}`)
        }).then((jsonData) => {
            if (jsonData != undefined)
                resolve(jsonData)
        }).catch((error) => {
            reject(error)
        })
    })
}

const fetchDetailsOfFirstToDo = (jsonData) => {

    let firstToDoId = jsonData[0].id;

    fetch(`https://jsonplaceholder.typicode.com/users/${firstToDoId}`).then((response) => {

        if (response.ok === true)
            return response.json()
    }).then((jsonData) => {
        if (jsonData != undefined) {
            console.log(`Details for first user in ToDo list is: `)
            console.log(jsonData)
        }
    })
}

fetchToDo().then((jsonData) => {
    fetchDetailsOfFirstToDo(jsonData)
}).catch((error) => {
    console.log(error)
})